# linuxScripts
Simple tools for Linux

## backup.sh

The script creates backup from /home and /etc folders:
- each day in separate folder
- each user folder in separate file

## maillog2email.sh

The script extracts e-mail addresses from the mail log to separate files.

## tuneup_fedora_28.sh

This script installs tools for java developers.
